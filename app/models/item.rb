class Item < ApplicationRecord
  has_many :emails, dependent: :destroy
  validates :threshold, numericality: { greater_than: 0 }
  validates :title, :success_msg, presence: true
  validate :end_date_not_in_past

  def verified_signups
    emails.where(verified: true)
  end

  def succeeded?
    if verified_signups.count >= threshold
      verified_signups.where(sent_success_msg: false).find_each(&:send_success_msg)
      return true
    end
    false
  end

  def sign_up(email_txt)
    email = Email.find_by(email: email_txt, item_id: id)
    if email
      email.send_verification
      return true
    else
      email = Email.new(email: email_txt, item_id: id)
      if email.save
        email.send_verification
        return true
      end
    end
    false
  end

  def ended?
    end_date && end_date <= Date.today
  end

  def self.hash_password(password)
    Digest::SHA256.base64digest(password || '')
  end

  private

    def end_date_not_in_past
      if end_date.present? && end_date == Date.today
        errors.add(:end_date, "can't be today")
      elsif end_date.present? && end_date < Date.today
        errors.add(:end_date, "can't be in the past")
      end
    end
end
