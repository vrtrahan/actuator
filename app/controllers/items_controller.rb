require 'digest'
class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update]
  before_action :check_edit_authorization, only: [:edit, :update]

  # GET /items
  def index
    @items = Item.where(public: true).order('created_at DESC').page(params[:page] || 1)
  end

  # GET /items/1
  def show
  end

  # GET /items/new
  def new
    @item = Item.new
  end

  # GET /items/1/edit
  def edit
  end

  def sign_up
    item = Item.find(params[:id])
    if item.sign_up(params[:email])
      redirect_to item_url(params[:id]), notice: 'Signed up - verify your email'
    else
      redirect_to item_url(params[:id]), notice: 'Error'
    end
  end

  def input_password
    @item = Item.find(params[:id])
    if Item.hash_password(params[:password]) == @item.password_hash
      session[:current_edit] = @item.id
      redirect_to edit_item_url(params[:id])
    else
      redirect_to item_url(params[:id]), notice: 'Wrong password'
    end
  end

  # POST /items
  def create
    item_params = item_create_params
    item_params[:password_hash] = Item.hash_password(params[:item][:password])
    item_params[:end_date] = end_date
    @item = Item.new(item_params)

    if @item.save
      redirect_to @item, notice: 'Item was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /items/1
  def update
    item_params = item_update_params
    item_params[:end_date] = end_date
    if @item.update(item_params)
      redirect_to @item, notice: 'Item was successfully updated.'
    else
      render :edit
    end
  end

  def remove
    email = Email.find_by(email: params[:email_remove], item_id: params[:id])
    email.send_removal_request if email
    redirect_to items_url, notice: 'A removal request has been sent, '\
      'if your email was signed up and verified for the item. If not, nothing has happened.'
  end

  private

    def set_item
      @item = Item.find(params[:id])
    end

    def item_update_params
      params.require(:item).permit(:success_msg, :public, :show_threshold, :show_verified_signups)
    end

    def item_create_params
      params.require(:item).permit(:title, :description, :threshold,
                                   :success_msg, :public, :show_threshold,
                                   :show_verified_signups)
    end

    def end_date
      return unless params[:item][:end_date_enabled] != '0'
      Date.new(params[:item]['end_date(1i)'].to_i,
               params[:item]['end_date(2i)'].to_i,
               params[:item]['end_date(3i)'].to_i)
    end

    def check_edit_authorization
      return unless !session[:current_edit] || session[:current_edit] != params[:id].to_i
      redirect_to item_url(params[:id]), notice: 'You aren\'t authorized to edit this item'
    end
end
