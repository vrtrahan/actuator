module Helpers
  module Verification
    extend Capybara::DSL
    class << self
      delegate :url_helpers, to: 'Rails.application.routes'
      def sign_up(item, email = 'foo@bar.baz')
        visit url_helpers.item_path(item)
        fill_in 'email', with: email
        perform_enqueued_jobs { click_on 'Sign up' }
      end

      def verify
        visit ActionMailer::Base.deliveries.first.body.to_s[/http:\/\/[a-z]+:?[0-9]*\/emails\/[0-9]+\/[0-9a-z]+/]
      end
    end
  end
end
