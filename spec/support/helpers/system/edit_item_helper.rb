module Helpers
  module EditItem
    extend Capybara::DSL
    class << self
      delegate :url_helpers, to: 'Rails.application.routes'

      def edit(item, &edits)
        submit_password(item, 'foo')
        yield if edits
        click_on 'Update Item'
        item.reload
      end

      def submit_password(item, password)
        visit url_helpers.item_path(item)
        fill_in 'password', with: password
        click_on 'Submit'
      end

      # any string that appears on the edit page and nowhere else
      def text_on_edit_page
        'This item has'
      end
    end
  end
end
