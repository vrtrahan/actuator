module Helpers
  module Actuation
    def self.create_emails(item, num, &block)
      emails = []
      num.times { emails.push(FactoryBot.create(:email, item: item)) }
      emails.each { |email| yield(email) } if block
      perform_enqueued_jobs { item.succeeded? }
      emails
    end

    def self.create_verified_emails(item, num)
      create_emails(item, num) do |email|
        email.verified = true
        email.save!
      end
    end
  end
end
