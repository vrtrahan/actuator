FactoryBot.define do
  factory :email do
    email { Faker::Internet.email }
    item
  end
end
