FactoryBot.define do
  factory :item do
    title { Faker::Company.bs }
    description { Faker::Lorem.paragraph }
    threshold { 1 }
    password_hash { Item.hash_password('foo') }
    success_msg { 'Success!' }
  end
end
