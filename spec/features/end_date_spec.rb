require 'rails_helper'

RSpec.feature 'End date' do
  it 'isn\'t required' do
    expect(FactoryBot.build(:item, end_date: nil)).to be_valid
  end

  it 'can\'t be today' do
    expect(FactoryBot.build(:item, end_date: Date.today)).to_not be_valid
  end

  it 'can\'t be in the past' do
    expect(FactoryBot.build(:item, end_date: Date.yesterday)).to_not be_valid
  end

  it 'can be added' do
    expect(FactoryBot.build(:item, end_date: Date.tomorrow)).to be_valid
  end

  it 'prevents signup after it passes' do
    item = FactoryBot.build(:item, end_date: Date.today)
    item.save(validate: false)
    expect(FactoryBot.build(:email, item: item)).to_not be_valid
  end

  it 'doesn\'t prevent signup before it passes' do
    item = FactoryBot.build(:item, end_date: Date.tomorrow)
    expect(FactoryBot.build(:email, item: item)).to be_valid
  end
end
