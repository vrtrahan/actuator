require 'rails_helper'
include ActiveJob::TestHelper
require 'support/helpers/system/verification_helper'

RSpec.feature 'Verification message' do
  let (:item) { FactoryBot.create(:item) }

  before :each do
    ActionMailer::Base.deliveries = []
  end

  it 'sends on signup', type: :mailer do
    Helpers::Verification.sign_up(item)
    expect(ActionMailer::Base.deliveries.count).to eq 1
  end

  it 'contains a link that verifies the associated email' do
    Helpers::Verification.sign_up(item)
    Helpers::Verification.verify
    expect(Email.find_by(email: 'foo@bar.baz', item: item).verified).to be true
  end

  it 'requires a verification token' do
    Helpers::Verification.sign_up(item)
    expect { visit ActionMailer::Base.deliveries.first.body.to_s[/http:\/\/[a-z]+:?[0-9]*\/emails\/[0-9]+\//] }
      .to raise_error(ActionController::RoutingError)
  end

  it 'requires the correct verification token' do
    Helpers::Verification.sign_up(item)
    visit ActionMailer::Base.deliveries.first.body.to_s[/http:\/\/[a-z]+:?[0-9]*\/emails\/[0-9]+\//] + SecureRandom.hex
    expect(Email.find_by(email: 'foo@bar.baz', item: item).verified).to be false
  end

  it 'creates a SendLog' do
    Helpers::Verification.sign_up(item)
    expect(SendLog.count).to eq 1
  end

  it 'creates multiple SendLogs on multiple signups' do
    2.times { Helpers::Verification.sign_up(item) }
    expect(SendLog.count).to eq 2
  end

  it 'sends a max of three verification emails per item per day' do
    4.times { Helpers::Verification.sign_up(item) }
    expect(ActionMailer::Base.deliveries.count).to eq 3
  end

  it 'ignores SendLogs from >24 hours ago' do
    3.times { Helpers::Verification.sign_up(item) }
    SendLog.all.each do |vsl|
      vsl.created_at = 1441.minutes.ago # 1440 minutes in a day
      vsl.updated_at = 1441.minutes.ago
      vsl.save!
    end
    Helpers::Verification.sign_up(item)
    expect(ActionMailer::Base.deliveries.count).to eq 4
  end

  it 'only counts VSLs per email' do
    3.times { Helpers::Verification.sign_up(item) }
    Helpers::Verification.sign_up(item, 'new@bar.baz')
    expect(ActionMailer::Base.deliveries.count).to eq 4
  end
end
