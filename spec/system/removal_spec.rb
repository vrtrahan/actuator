require 'rails_helper'
include ActiveJob::TestHelper
require 'support/helpers/system/removal_helper'

RSpec.feature 'Signup removal' do
  let (:item) { FactoryBot.create(:item) }
  let (:email) { FactoryBot.create(:email, email: 'foo@bar.baz', item: item) }

  before :each do
    ActionMailer::Base.deliveries = []
  end

  it 'sends a removal email if the signup is verified' do
    Helpers::Removal.verify(email)
    Helpers::Removal.request_removal(email.email, item)
    expect(ActionMailer::Base.deliveries.count).to eq 1
  end

  it 'creates a SendLog' do
    Helpers::Removal.verify(email)
    Helpers::Removal.request_removal(email.email, item)
    expect(SendLog.count).to eq 1
  end

  it 'doesn\'t send a removal email if there\'s no signup' do
    Helpers::Removal.request_removal('bork@bork.bork', item)
    expect(ActionMailer::Base.deliveries.count).to eq 0
  end

  it 'doesn\'t send a removal email if the signup isn\'t verified' do
    Helpers::Removal.request_removal(email.email, item)
    expect(ActionMailer::Base.deliveries.count).to eq 0
  end

  it 'contains a link that will remove a verified signup' do
    Helpers::Removal.verify(email)
    Helpers::Removal.request_removal(email.email, item)
    visit ActionMailer::Base.deliveries.first.body.to_s[/http:\/\/[a-z]+:?[0-9]*\/emails\/[0-9]+\/[0-9a-z]+\/remove/]
    expect(item.emails.count).to eq 0
  end

  it 'won\'t send more than three times in a day' do
    Helpers::Removal.verify(email)
    4.times { Helpers::Removal.request_removal(email.email, item) }
    expect(ActionMailer::Base.deliveries.count).to eq 3
  end
end
