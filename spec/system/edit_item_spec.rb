require 'rails_helper'
require 'support/helpers/system/edit_item_helper'

RSpec.feature 'Edit item' do
  let (:item) { FactoryBot.create(:item, password_hash: Item.hash_password('foo')) }

  it 'requires a correct password' do
    Helpers::EditItem.submit_password(item, 'wrong password')
    expect(page).to_not have_content('This item has')
  end

  it 'redirects from the edit page if no password was supplied' do
    visit edit_item_path(item)
    expect(page).to_not have_content(Helpers::EditItem.text_on_edit_page)
  end

  it 'accepts a correct password' do
    Helpers::EditItem.submit_password(item, 'foo')
    expect(page).to have_content(Helpers::EditItem.text_on_edit_page)
  end

  it 'allows editing of Public attr' do
    Helpers::EditItem.edit(item) { uncheck 'Public' }
    expect(item.public).to be false
  end

  it 'allows editing of Show Threshold attr' do
    Helpers::EditItem.edit(item) { uncheck 'Show threshold' }
    expect(item.show_threshold).to be false
  end

  it 'allows editing of Show Verified Signups attr' do
    Helpers::EditItem.edit(item) { check 'Show verified signups' }
    expect(item.show_verified_signups).to be true
  end

  it 'allows editing of the success message' do
    Helpers::EditItem.edit(item) { fill_in 'item_success_msg', with: 'asdf' }
    expect(item.success_msg).to eq 'asdf'
  end

  it 'doesn\'t add an end date if the user doesn\'t mean to add one' do
    Helpers::EditItem.edit(item) { uncheck 'Public' } # whatever
    expect(item.end_date).to be nil
  end

  it 'allows removing an end date' do
    item.end_date = Date.new(2024, 1, 1)
    item.save!
    Helpers::EditItem.edit(item) { uncheck 'item_end_date_enabled' }
    expect(item.end_date).to be nil
  end

  it 'allows adding an end date' do
    Helpers::EditItem.edit(item) do
      check 'item_end_date_enabled'
      select '2024',    from: 'item_end_date_1i'
      select 'January', from: 'item_end_date_2i'
      select '1',       from: 'item_end_date_3i'
    end
    expect(item.end_date).to eq Date.new(2024, 1, 1)
  end

  it 'displays the date selectors' do
    item.end_date = Date.new(2024, 1, 1)
    item.save!
    visit item_path(item)
    fill_in 'password', with: 'foo'
    click_on 'Submit'
    expect(page).to have_select('item_end_date_1i')
    expect(page).to have_select('item_end_date_2i')
    expect(page).to have_select('item_end_date_3i')
  end

  it 'loads the item\'s end date if it has one' do
    item.end_date = Date.new(2024, 1, 1)
    item.save!
    visit item_path(item)
    fill_in 'password', with: 'foo'
    click_on 'Submit'
    expect(find('#item_end_date_1i').value).to eq '2024'
    expect(find('#item_end_date_2i').value).to eq '1'
    expect(find('#item_end_date_3i').value).to eq '1'
  end

  it 'loads absence of an end date if the item doesn\'t have one' do
    visit item_path(item)
    fill_in 'password', with: 'foo'
    click_on 'Submit'
    expect(page).to have_unchecked_field('item_end_date_enabled')
  end
end
