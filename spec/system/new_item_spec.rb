require 'rails_helper'
require 'support/helpers/system/new_item_helper'

RSpec.feature 'New item' do
  it 'exists' do
    visit new_item_path
    expect(page).to have_content 'New Item'
  end

  it 'allows creation of an item' do
    Helpers::NewItem.build_new_item
    expect(Item.first.title).to eq 'Test title'
  end

  it 'hashes passwords correctly' do
    Helpers::NewItem.build_new_item
    expect(Item.first.password_hash).to eq Item.hash_password('foo')
  end

  it 'allows a nil end date' do
    Helpers::NewItem.build_new_item { uncheck 'item_end_date_enabled' }
    expect(Item.first.end_date).to eq nil
  end

  it 'allows a non-nil end date' do
    # note that this also checks to make sure you can add an end date with JS disabled
    Helpers::NewItem.build_new_item do
      check 'item_end_date_enabled'
      select '2024',    from: 'item_end_date_1i'
      select 'January', from: 'item_end_date_2i'
      select '1',       from: 'item_end_date_3i'
    end
    expect(Item.first.end_date).to eq Date.new(2024, 1, 1)
  end

  it 'requires a title' do
    Helpers::NewItem.build_new_item(title: '')
    expect(Item.count).to eq 0
  end

  it 'requires a success message' do
    Helpers::NewItem.build_new_item(success_msg: '')
    expect(Item.count).to eq 0
  end
end
