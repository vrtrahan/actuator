class AddEndDateToItems < ActiveRecord::Migration[5.1]
  def change
  	add_column :items, :end_date, :date, default: nil
  end
end
