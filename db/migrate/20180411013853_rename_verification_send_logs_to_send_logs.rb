class RenameVerificationSendLogsToSendLogs < ActiveRecord::Migration[5.1]
  def change
  	rename_table :verification_send_logs, :send_logs
  end
end
