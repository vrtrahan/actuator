class AddEmailUniquenessPerItem < ActiveRecord::Migration[5.1]
  def change
  	add_index :emails, [:email, :item_id], unique: true
  end
end
