# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180411013853) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "emails", force: :cascade do |t|
    t.string "email", null: false
    t.boolean "verified", null: false
    t.string "verification_token", null: false
    t.integer "item_id", null: false
    t.boolean "sent_success_msg", default: false, null: false
    t.index ["email", "item_id"], name: "index_emails_on_email_and_item_id", unique: true
  end

  create_table "items", force: :cascade do |t|
    t.string "title", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "threshold", null: false
    t.string "password_hash"
    t.text "success_msg", null: false
    t.boolean "show_threshold", default: true, null: false
    t.boolean "show_verified_signups", default: false, null: false
    t.boolean "public", default: true, null: false
    t.date "end_date"
  end

  create_table "send_logs", force: :cascade do |t|
    t.integer "email_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "emails", "items"
end
